package com.example.shashankmisra.play.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public final class SongsDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "songs.db";
    private static final String KEY_ID = "_id";

    public static final String SONGS_TABLE = "songs";
    public static final String SONG_NAME = "name";
    public static final String SONG_PATH = "path";
    public static final String SONG_ALBUM = "album";
    public static final String SONG_ARTIST = "artist";
    public static final String SONG_DURATION = "duration";
    public static final String SONG_GENRE = "genre";
    public static final String SONG_TITLE = "title";


    public SongsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(
                "create table " + SONGS_TABLE + " " +
                        "(" + KEY_ID + " integer primary key autoincrement, " +
                        SONG_NAME + " text not null, " +
                        SONG_PATH + " text not null, " +
                        SONG_ALBUM + " text, " +
                        SONG_DURATION + " text, " +
                        SONG_GENRE + " text, " +
                        SONG_TITLE + " text, " +
                        SONG_ARTIST + " text )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.d("msg", "in upgrade");
        db.execSQL("DROP TABLE IF EXISTS " + SONGS_TABLE);
        onCreate(db);
    }
}

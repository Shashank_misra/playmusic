package com.example.shashankmisra.play;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class MApp extends Application {

    public static interface TAGS{
        String FIRST_START = "fStart";
    }


    private static MApp instanceVar;
    SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        instanceVar = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static MApp getInstanceVar() {
        return instanceVar;
    }

    public static void cL(String msg) {
        Log.d("mApp", msg);
    }

    public static void cL(String tag, String msg) {
        Log.d(tag, msg);
    }

    public String getSPString(String pName) {

        return preferences.getString(pName, "");
    }

    public void setSPString(String pName, String val) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(pName, val);
        editor.apply();
    }

    public boolean getSPBoolean(String pName){
        return preferences.getBoolean(pName, false);
    }

    public void setSPBoolean(String pName, boolean val){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(pName, val);
        editor.apply();
    }
}

package com.example.shashankmisra.play;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.example.shashankmisra.play.DB.DBM_Songs;
import com.example.shashankmisra.play.Utils.TextViewPlus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class SplashActivity extends AppCompatActivity {


    final int REQUEST_CODE = 12;
    private String MEDIA_PATH = Environment.getExternalStorageDirectory()
            .getPath() + "/";

    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

    ProgressBar progressBar;
    TextViewPlus status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = (ProgressBar) findViewById(R.id.progress);
        status = (TextViewPlus) findViewById(R.id.status);
        findViewById(R.id.proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        });

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                Dialog d = new Dialog(this);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.dialog_request_readstorage_permission);
                d.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                REQUEST_CODE);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE);
            }
        } else {

            File[] paths = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
            MApp.cL(paths[0].getPath());
            if (paths.length > 1) MApp.cL(paths[1].getPath());

            String p1 = paths[0].getPath();
            MApp.cL(p1);
            int ind = p1.indexOf("Android");
            p1 = p1.replace(p1.substring(ind), "");

            String p2 = paths[1].getPath();
            MApp.cL(p2);
            ind = p2.indexOf("Android");
            p2 = p2.replace(p2.substring(ind), "");

            final String finalP1 = p1;
            final String finalP2 = p2;
            Thread scan = new Thread(new Runnable() {
                @Override
                public void run() {

                    MEDIA_PATH = finalP1;
                    getPlayList();
                    MEDIA_PATH = finalP2;
                    getPlayList();

                    MApp.cL("size : " + songsList.size());

                    status.post(new Runnable() {
                        @Override
                        public void run() {
                            Animation fadeOutAnimation = AnimationUtils.
                                    loadAnimation(SplashActivity.this, R.anim.fade_out);
                            fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    findViewById(R.id.scanCard).setVisibility(View.GONE);
                                    ((TextViewPlus) findViewById(R.id.message)).setText("Found " +
                                            songsList.size() + " Songs");
                                    findViewById(R.id.msgCard).setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });
                            findViewById(R.id.scanCard).startAnimation(fadeOutAnimation);
                        }
                    });
                }
            });
            scan.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    File[] paths = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
                    MApp.cL(paths[0].getPath());
                    if (paths.length > 1) MApp.cL(paths[1].getPath());

                    String p1 = paths[0].getPath();
                    MApp.cL(p1);
                    int ind = p1.indexOf("Android");
                    p1 = p1.replace(p1.substring(ind), "");

                    String p2 = paths[1].getPath();
                    MApp.cL(p2);
                    ind = p2.indexOf("Android");
                    p2 = p2.replace(p2.substring(ind), "");

                    final String finalP1 = p1;
                    final String finalP2 = p2;
                    Thread scan = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            MEDIA_PATH = finalP1;
                            getPlayList();
                            MEDIA_PATH = finalP2;
                            getPlayList();

                            status.post(new Runnable() {
                                @Override
                                public void run() {
                                    Animation fadeOutAnimation = AnimationUtils.
                                            loadAnimation(SplashActivity.this, R.anim.fade_out);
                                    fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            findViewById(R.id.scanCard).setVisibility(View.GONE);
                                            ((TextViewPlus) findViewById(R.id.message)).setText("Found " +
                                                    songsList.size() + " SongsDB");
                                            findViewById(R.id.msgCard).setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                    findViewById(R.id.scanCard).startAnimation(fadeOutAnimation);
                                }
                            });
                        }
                    });
                    scan.start();

                } else {

                    AlertDialog dialog = new AlertDialog.Builder(this).
                            setTitle("Permissions Required").
                            setMessage("Permissions are required to read songs on device").
                            setIcon(R.drawable.ic_block_black_24dp).
                            setCancelable(false).
                            setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    ActivityCompat.requestPermissions(SplashActivity.this,
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            REQUEST_CODE);
                                }
                            }).
                            setNegativeButton("Exit App", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    finish();
                                }
                            }).
                            create();
                    dialog.show();
                }
            }
        }
    }

    /**
     * Function to read all mp3 files and store the details in
     * ArrayList
     */
    public ArrayList<HashMap<String, String>> getPlayList() {

        if (MEDIA_PATH != null) {
            File home = new File(MEDIA_PATH);
            File[] listFiles = home.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (final File file : listFiles) {
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addSongToList(file);
                    }
                }
            }
        }
        return songsList;
    }

    private void scanDirectory(File directory) {

        switch (directory.getName()) {
            case "Android":
            case "Documents":
            case "DCIM":
                break;

            default:
                File[] listFiles = directory.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (File file : listFiles) {
                        if (file.isDirectory()) {
                            scanDirectory(file);
                        } else {
                            addSongToList(file);
                        }
                    }
                }
                break;
        }
    }

    private void addSongToList(final File song) {
        String mp3Pattern = ".mp3";
        if (song.getName().endsWith(mp3Pattern)) {

            HashMap<String, String> songMap = new HashMap<>();
            songMap.put("songTitle",
                    song.getName().substring(0, (song.getName().length() - 4)));
            songMap.put("songPath", song.getPath());
            // Adding each song to SongList
            songsList.add(songMap);
            DBM_Songs dbm = new DBM_Songs(this);

            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(song.getPath());
            String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            String genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
            final String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

            status.post(new Runnable() {
                @Override
                public void run() {
                    status.setText("Scanning: " + title);
                }
            });
            if(title != null) MApp.cL("<"+title+">");
            if (title != null && title.length() > 0)
                dbm.addSong(songMap.get("songTitle"), songMap.get("songPath"), albumName, artist, duration, genre, title);
        }
    }
}


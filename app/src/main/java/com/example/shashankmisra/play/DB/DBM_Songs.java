package com.example.shashankmisra.play.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.shashankmisra.play.Beans.Song;
import com.example.shashankmisra.play.MApp;

import java.util.ArrayList;

public class DBM_Songs {

    Context workContext;
    SongsDB worker;

    public DBM_Songs() {
        workContext = MApp.getInstanceVar().getApplicationContext();
        worker = new SongsDB(workContext);
    }

    public DBM_Songs(Context context) {
        workContext = context;
        worker = new SongsDB(context);
    }

    public int addSong(String name, String path) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SongsDB.SONG_NAME, name);
        cv.put(SongsDB.SONG_PATH, path);

        long res = db.insert(SongsDB.SONGS_TABLE, null, cv);
        db.close();
        return (int) res;
    }

    public ArrayList<Song> getAllSongs() {

        ArrayList<Song> names = new ArrayList<>();
        SQLiteDatabase db = worker.getReadableDatabase();
        Cursor res = db.query(true, SongsDB.SONGS_TABLE, new String[]{SongsDB.SONG_TITLE, SongsDB.SONG_GENRE,
                        SongsDB.SONG_DURATION, SongsDB.SONG_NAME, SongsDB.SONG_ALBUM, SongsDB.SONG_ARTIST,
                        SongsDB.SONG_PATH},
                null, null, null, null, null, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {

            Song s = new Song();
            s.setAlbum(res.getString(res.getColumnIndex(SongsDB.SONG_ALBUM)));
            s.setTitle(res.getString(res.getColumnIndex(SongsDB.SONG_TITLE)));
            s.setArtist(res.getString(res.getColumnIndex(SongsDB.SONG_ARTIST)));
            s.setGenre(res.getString(res.getColumnIndex(SongsDB.SONG_GENRE)));
            s.setDuration(res.getString(res.getColumnIndex(SongsDB.SONG_DURATION)));
            s.setName(res.getString(res.getColumnIndex(SongsDB.SONG_NAME)));
            s.setPath(res.getString(res.getColumnIndex(SongsDB.SONG_PATH)));

            names.add(s);
            res.moveToNext();
        }
        return names;
    }

    public int addSong(String name, String path, String album, String artist, String duration,
                       String genre, String title) {

        SQLiteDatabase db = worker.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(SongsDB.SONG_NAME, name);
        cv.put(SongsDB.SONG_PATH, path);
        cv.put(SongsDB.SONG_ALBUM, album);
        cv.put(SongsDB.SONG_ARTIST, artist);
        cv.put(SongsDB.SONG_DURATION, duration);
        cv.put(SongsDB.SONG_GENRE, genre);
        cv.put(SongsDB.SONG_TITLE, title.trim());

        long res = db.insert(SongsDB.SONGS_TABLE, null, cv);
        db.close();
        return (int) res;
    }
}
package com.example.shashankmisra.play.Beans;

import android.support.annotation.NonNull;

public class Song implements Comparable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    private String name, title, album, artist, duration, path, genre;

    @Override
    public int compareTo(@NonNull Object o) {

        Song s = (Song) o;
        String n1 = getTitle();
        String n2 = s.getTitle();
        if (n1 != null && n2 != null)
            return n1.trim().toLowerCase().compareTo(n2.trim().toLowerCase());
        else if (n1 != null)
            return 1;
        else if (n2 != null)
            return -1;
        else
            return 0;
    }
}
